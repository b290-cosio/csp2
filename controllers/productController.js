const Product = require("../models/Product");


module.exports.addProduct = (reqBody) => {

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newProduct.save()
	.then(product => true)
	.catch(err => false)
	
}
module.exports.getAllProducts = (req, res) => {
	
	return Product.find({}).then(result => result)
	.catch(err =>{

		console.log(err);

		return false;
	});
};

module.exports.getAllActiveProducts = (req, res) =>{

	return Product.find({ isActive : true }).then(result => result)
			.catch(err =>{

				console.log(err);

				return false;
			})
};

module.exports.getProduct = reqParams => {

	return Product.findById(reqParams.productId).then(result => result)
		.catch(err =>{

			console.log(err)

			return false;
		});
}

module.exports.updateProduct = (reqParams, reqBody) => {
  let updatedProduct = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price
  };

  return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
    .then(result => {
      return result;
    })
    .catch(err => {
      console.log(err);
      return false;
    });
};

module.exports.archiveProduct = (productId) => {
  let archivedProduct = { isActive: false };

  return Product.findByIdAndUpdate(productId, archivedProduct, { new: true })
    .then(archivedProduct => {
      if (!archivedProduct) {
        return false;
      }
      return archivedProduct;
    })
    .catch(err => {
      console.log(err);
      return false;
    });
};

module.exports.activateProduct = (productId) => {
  return Product.findByIdAndUpdate( productId, { isActive: true },{ new: true })
    .then(activatedProduct => {
      if (!activatedProduct) {
        return false; 
      }
      return activatedProduct;
    })
    .catch(err => {
      console.log(err);
      return false; 
    });
};
