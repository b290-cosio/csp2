const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');

module.exports.createOrder = async (data, isAdmin) => {
  if (isAdmin) {
    console.log('Admin users cannot perform checkout');
    return false;
  }

  let user = await User.findById(data.userId);
  if (!user) {
    console.log('User not found');
    return false;
  }

  let product = await Product.findById(data.productId);
  if (!product) {
    console.log('Product not found');
    return false;
  }

  let quantity = data.quantity;
  let subtotal = product.price * quantity;
  let totalAmount = subtotal;

  let newOrder = new Order({
    userId: data.userId,
    products: [{
      productId: data.productId,
      quantity: quantity,
      subtotal: subtotal
    }],
    totalAmount: totalAmount
  });

  let savedOrder = await newOrder.save();
  if (savedOrder) {
    console.log("Order created successfully");
    return true;
  } else {
    console.log("Failed to create order");
    return false;
  }
};

module.exports.orderGetAll = () => {
  return Order.find()
    .then(orders => {
      if (orders.length === 0) {
        console.log("No orders are currently listed");
        return "No orders available";
      } else {
        return orders;
      }
    })
    .catch(error => {
      console.log(error);
      return false;
    });
};