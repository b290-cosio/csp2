const bcrypt = require("bcrypt");

const auth = require("../auth");
const User = require("../models/User")
const Order = require("../models/Order");
const Product = require("../models/Product");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email:reqBody.email}).then(result=>{
		if(result.length>0){
			return true;
		}else{
			return false;
		}
	}).catch(err=>{
		console.log(err);
		return false;
	})
};


module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10),
		mobileNo : reqBody.mobileNo		
	});
	return newUser.save()
		.then(user => {return true}).catch(err => {return false});
}


module.exports.loginUser = (data) => {
	return User.findOne({ email : data.email }).then(result => {
		if(result === null){
			console.log("user is not registered")
			return false   	
		   }else {
			const isPasswordCorrect = bcrypt.compareSync(data.password, result.password);	
			if(isPasswordCorrect){
				return { access : auth.createAccessToken(result) }
			}else {
				console.log("password did not match")
				return false
			}
		}
	}).catch(err =>{
		console.log(err)
		return false
	})
}


module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {		
		if(result === null){
			console.log("user is not registered")
			return false
		}else{
		result.password = "";
		return result	}
	}).catch(err =>{
		console.log(err)
		return false
	})
};


module.exports.setAdmin = (userEmail) => {
  return User.findOne({ email: userEmail })
    .then(user => {
      if (!user) {
        console.log("User not found");
        return false;
      }

      user.isAdmin = true;
      return user.save();
    })
    .catch(error => {
      console.log(error);
      return false;
    });
};

module.exports.userGetAuthUser = (reqBody) => {
  return Order.find({ userId: reqBody.userId })
    .then(orders => {
      if (orders.length === 0) {
        return "No order list available";
      } else {
        return orders;
      }
    })
    .catch(error => {
      console.log(error);
      return false;
    });
};