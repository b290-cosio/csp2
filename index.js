const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const productRoute = require("./routes/productRoute");
const userRoute = require("./routes/userRoute");
const orderRoute = require ("./routes/orderRoute")

const app = express();

mongoose.connect("mongodb+srv://admin:admin123@zuitt.pzexkiy.mongodb.net/e-commerce?retryWrites=true&w=majority",
{
	useNewUrlParser : true,
	useUnifiedTopology : true
}
);
//Middlewares
mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas."))
app.use(express.urlencoded({ extended : true}));
app.use(express.json());
app.use(cors())

//Routes
app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute)

if(require.main === module){
	app.listen(process.env.PORT || 4000,() =>{
		console.log(`API is now on port ${process.env.PORT || 4000}`)
	});

}		



module.exports = { app, mongoose };