const express = require('express');
const auth = require ("../auth")

const router = express.Router();
const orderController = require('../controllers/orderController');

router.post("/checkout", auth.verify, (req, res) => {
  let userId = auth.decode(req.headers.authorization).id;
  let { productId, quantity } = req.body;

  let data = {
    userId,
    productId,
    quantity
  };
  
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;

  if (isAdmin) {
    res.send({ error: "Admin users cannot perform checkout" });
  } else {
    orderController.createOrder(data)
      .then(resultFromController => res.send(resultFromController))
      .catch(err => {
        console.log(err);
        res.send({ error: "Failed to create order" });
      });
  }
});

router.get("/getAll", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;

  if (isAdmin) {
    orderController.orderGetAll()
      .then(result => res.send(result))
      .catch(error => {
        console.log(error);
          return false;
      });
  } else {
    console.log("Admin permission is required")
    res.send("Authentication failed, user is not an admin");
  }
});


module.exports = router;