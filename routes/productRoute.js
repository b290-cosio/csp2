const express = require("express");
const router = express.Router();

const auth = require("../auth");
const productController = require("../controllers/productController");


router.post("/add", auth.verify, (req, res) => {

  const userData = auth.decode(req.headers.authorization);

  if(userData.isAdmin){
    productController.addProduct(req.body)
    .then(resultFromController => res.send(resultFromController));
  } else {
    console.log({ auth : "unauthorized user"})
    res.send("Product cant be added, User must be an admin")
  };  

});

router.get("/all", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    productController.getAllProducts()
      .then(resultFromController => res.send(resultFromController))
      .catch(err => {
        console.log(err);
      });
  } else {
    console.log("Authentication failed ,Admin Permission is required")
    return res.send("Admin permission is required");
  }
});

router.get("/", (req, res) => {
  productController.getAllActiveProducts()
    .then(resultFromController => res.send(resultFromController))
    .catch(err => {
      console.log(err);
      return false;
    });
});

router.get("/:productId", (req, res) => {
  console.log(req.params);
  productController.getProduct(req.params)
    .then(resultFromController => res.send(resultFromController))
    .catch(err => {
      console.log(err);
      return false;
    });
});

router.put("/:productId", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    console.log("isAdmin: true");
    productController.updateProduct(req.params, req.body)
      .then(resultFromController => {
        console.log("resultFromController:", resultFromController);
        res.send(resultFromController);
      })
      .catch(err => {
        console.log(err);
        return false;
      });
  } else {
    res.send("Admin permission is required");
  }
});

router.patch("/:productId/archive", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    productController.archiveProduct(req.params.productId)
      .then(resultFromController => res.send(resultFromController))
      .catch(err => {
        console.log(err);
        res.send({ error: "Failed to archive product" });
      });
  } else {
    res.send("Admin permission is required");
  }
});

router.patch("/:productId/activate", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  
  if (userData.isAdmin) {
    productController.activateProduct(req.params.productId)
      .then(resultFromController => res.send(resultFromController))
      .catch(err => {
        console.log(err);
        return false;
      });
  } else {
    res.send("Admin permission is required");
  }
});


module.exports = router;