const express = require('express');
const router = express.Router();

const auth = require("../auth");
const userController = require('../controllers/userController');


router.post("/checkEmail", (req, res)=>{
  userController.checkEmailExists(req.body)
    .then(resultFromController => res.send(resultFromController));
  });


router.post("/register", (req, res) => {
  userController.registerUser(req.body)
    .then(resultFromController => res.send(resultFromController))
    .catch(err => {
      console.log(err);
      return false;
    });
});


router.post("/login", (req,res) =>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
		.catch(err =>{

			console.log(err);

			return false;
		})
});


router.get("/details", auth.verify, (req, res) =>{
		const userData = auth.decode(req.headers.authorization);
		userController.getProfile({ userId : userData.id })
	.then(resultFromController => res.send(resultFromController))
	.catch(err =>{
		console.log(err);
		return false
	})

});

router.put('/set-admin', auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (!userData || !userData.isAdmin) {
    console.log("Admin permission is required to set a user as admin");
    return res.send("Admin permission is required to set a user as admin");
  }

  const userEmail = req.body.userEmail;

  userController.setAdmin(userEmail, userData)
    .then(result => {
      if (!result) {
        console.log("User not found");
        return res.send("User not found");
      }
      console.log("User set as admin successfully");
      return res.send(result);
    })
    .catch(error => {
      console.log(error);
      return res.send("Error setting user as admin");
    });
});

router.get("/myOrders", auth.verify, (req, res) => {
  let userData = {
    userId: auth.decode(req.headers.authorization).id
  };

  userController.userGetAuthUser(userData)
    .then(result => res.send(result))
    .catch(error => {
      console.log(error);
      res.send("Failed to retrieve user orders" );
    });
});


module.exports = router;
