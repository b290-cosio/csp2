const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "name is required"]
  },
  description: {
    type: String,
    required: [true, "description required"]
  },
  price: {
    type: Number,
    required: [true, "Price is required"]
  },
  isActive: {
    type: Boolean,
    default: true
  },
  createdOn: {
    type: Date,
    default: Date.now
  },
  userOrders: [
    {
      userId: {
        type: String,
      },
      orderId: {
        type: String,
      }
    }
  ]
});

module.exports = mongoose.model("Product", productSchema);